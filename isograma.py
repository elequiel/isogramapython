def isograma(palavra):
    num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    palavra1 = set(palavra)
    for letra in palavra1:
        if letra in num:
            return (False, "# existe um numero")
    if len(palavra) == len(palavra1):
        return (palavra, True)
    else:
        return (palavra, False)


palavra = input("Digite uma palavra: ")
print(isograma(palavra.lower()))
