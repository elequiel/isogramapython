# isogramaPython

script para verificar se uma palavra é um isograma ( se a mesma não repete nenhuma letra )

# Problema

Um isograma é uma palavra que não tem letras repetidas, consecutivas ou não consecutivas.
Implemente uma função que determine se uma string que contém apenas letras é um isograma e retorne um booleano, indicando True para um isograma e False para não-isogramas.
Suponha que a string vazia seja um isograma. Ignore o caso de letra (case insensitive).

### Exemplos:
```
is_isogram("Dermatoglyphics" ) ==> True
is_isogram("aba" )             ==> False
is_isogram("moOse" )           ==> False # existe um número
